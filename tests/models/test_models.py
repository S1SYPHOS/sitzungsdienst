"""
This module is part of the 'sitzungsdienst' package,
which is released under GPL-3.0-only license.
"""

from sitzungsdienst.models import Date, format_people, format_person

# Global setup
# (1) Generic dates
result1 = Date(
    {
        "title": "Ref'in",
        "doc": "",
        "first": "Una",
        "last": "Persona",
        "department": "(123)",
    },
    [],
)

result2 = Date(
    {
        "title": "Ref",
        "doc": "Dr.",
        "first": "Max",
        "last": "Mustermann",
        "department": "(456)",
    },
    [
        {
            "title": "StA'in",
            "doc": "",
            "first": "Multipla",
            "last": "Persona",
            "department": "",
        },
    ],
)


def test_date_name():
    """
    Tests 'date.name'
    """

    # Assert result
    assert result1.name == "Ref'in Una Persona (123)"
    assert result2.name == "Ref Dr. Max Mustermann (456)"


def test_date_name2sort():
    """
    Tests 'date.name2sort'
    """

    # Assert result
    assert result1.name2sort == "Persona, Una"
    assert result2.name2sort == "Mustermann, Max"


def test_date_assigned():
    """
    Tests 'date.assigned'
    """

    # Assert result
    assert result1.assigned == "Ref'in Una Persona (123)"
    assert result2.assigned == "Ref Dr. Max Mustermann (456); StA'in Multipla Persona"


def test_format_people():
    """
    Tests 'format_people'
    """

    # Assert result
    assert (
        format_people(
            [
                {
                    "title": "Ref",
                    "doc": "Dr.",
                    "first": "Max",
                    "last": "Mustermann",
                    "department": "(456)",
                },
                {
                    "title": "StA'in",
                    "doc": "",
                    "first": "Multipla",
                    "last": "Persona",
                    "department": "",
                },
            ]
        )
        == "Ref Dr. Max Mustermann (456); StA'in Multipla Persona"
    )


def test_format_person():
    """
    Tests 'format_person'
    """

    # Assert result #1
    assert (
        format_person(
            {
                "title": "EStA'in",
                "doc": "Dr.",
                "first": "Marie",
                "last": "Mustermann",
                "department": "",
            }
        )
        == "EStA'in Dr. Marie Mustermann"
    )

    # Assert result #2
    assert (
        format_person(
            {
                "title": "Ref'in",
                "doc": "",
                "first": "Una",
                "last": "Persona",
                "department": "(123)",
            }
        )
        == "Ref'in Una Persona (123)"
    )
