"""
This module is part of the 'sitzungsdienst' package,
which is released under GPL-3.0-only license.
"""

from sitzungsdienst import COURT_DATES, EXPRESS_DATES, PERSON
from sitzungsdienst.regex import get_court_dates, get_express_dates, get_people


def test_regex_court_dates():
    """
    Tests regex 'COURT_DATES'
    """

    # Assert result #1
    assert COURT_DATES.search(
        "- 09:00 445 Js 36865/19 Pistorius, Adalbert , OAA"
    ).groupdict() == {
        "where": "-",
        "time": "09:00",
        "docket": "445 Js 36865/19",
        "assigned": "Pistorius, Adalbert , OAA",
    }

    # Assert result #2
    # Mehrere Sitzungsvertreter:innen
    assert COURT_DATES.search(
        "Strafkammer XVI- IV, 2. OG 09:00 620 Js 341/21 "
        + "Lederer, Chloe , StA'in Böhm , Lara, StA'in"
    ).groupdict() == {
        "where": "Strafkammer XVI- IV, 2. OG",
        "time": "09:00",
        "docket": "620 Js 341/21",
        "assigned": "Lederer, Chloe , StA'in Böhm , Lara, StA'in",
    }

    # Assert result #3
    # Mehrere Termine
    assert [
        item.groupdict()
        for item in COURT_DATES.finditer(
            "09:00 80 Js 330312/21 Bader, Alina-Karla , StA'in "
            + "10:30 511 Js 8473/21 Bader, Alina-Karla , StA'in VIII, Holzmarkt 6 "
            + "11:00 631 Js 33181/20 Bader, Alina-Karla , StA'in "
            + "11:45 14 Js 19333/20 Bader, Alina-Karla , StA'in"
        )
    ] == [
        {
            "where": "",
            "time": "09:00",
            "docket": "80 Js 330312/21",
            "assigned": "Bader, Alina-Karla , StA'in ",
        },
        {
            "where": "",
            "time": "10:30",
            "docket": "511 Js 8473/21",
            "assigned": "Bader, Alina-Karla , StA'in ",
        },
        {
            "where": "VIII, Holzmarkt 6",
            "time": "11:00",
            "docket": "631 Js 33181/20",
            "assigned": "Bader, Alina-Karla , StA'in ",
        },
        {
            "where": "",
            "time": "11:45",
            "docket": "14 Js 19333/20",
            "assigned": "Bader, Alina-Karla , StA'in",
        },
    ]


def test_get_court_dates():
    """
    Tests 'get_court_dates'
    """

    # Assert result #1
    assert get_court_dates("") == []

    # Assert result #2
    assert get_court_dates("- 09:00 445 Js 36865/19 Pistorius, Adalbert , OAA") == [
        {
            "where": "-",
            "time": "09:00",
            "docket": "445 Js 36865/19",
            "assigned": "Pistorius, Adalbert , OAA",
        }
    ]


def test_regex_express():
    """
    Tests regex 'EXPRESS_DATES'
    """

    # Assert result #1
    assert EXPRESS_DATES.search(
        "26.11.2021 - 29.11.2021 Plattner, Adalbert , EOAA "
    ).groupdict() == {
        "start": "26.11.2021",
        "end": "29.11.2021",
        "assigned": "Plattner, Adalbert , EOAA ",
    }

    # Assert result #2
    assert EXPRESS_DATES.search(
        "29.11.2021 - 03.12.2021 Häberling, Max , StA "
    ).groupdict() == {
        "start": "29.11.2021",
        "end": "03.12.2021",
        "assigned": "Häberling, Max , StA ",
    }

    # Assert result #3
    assert EXPRESS_DATES.search(
        "03.12.2021 - 06.12.2021 Dr. Eisner, Julien , StA"
    ).groupdict() == {
        "start": "03.12.2021",
        "end": "06.12.2021",
        "assigned": "Dr. Eisner, Julien , StA",
    }


def test_get_express_dates():
    """
    Tests 'get_express_dates'
    """

    # Assert result #1
    assert get_express_dates("") == []

    # Assert result #2
    assert get_express_dates("26.11.2021 - 29.11.2021 Plattner, Adalbert , EOAA ") == [
        {
            "start": "26.11.2021",
            "end": "29.11.2021",
            "assigned": "Plattner, Adalbert , EOAA",
        }
    ]


def test_regex_person():
    """
    Tests regex 'PERSON'
    """

    # Assert result #1
    # Staatsanwältin
    assert PERSON.search("Bobbele, Alina , StA'in").groupdict() == {
        "title": "StA'in",
        "doc": "",
        "first": "Alina",
        "last": "Bobbele",
        "department": "",
    }

    # Assert result #2
    # Staatsanwalt
    assert PERSON.search("Müller, Peter , StA").groupdict() == {
        "title": "StA",
        "doc": "",
        "first": "Peter",
        "last": "Müller",
        "department": "",
    }

    # Assert result #3
    # Regierungsrätin als Amtsanwältin
    assert PERSON.search("Meier, Anita , RR'inaAA'in").groupdict() == {
        "title": "RR'inaAA'in",
        "doc": "",
        "first": "Anita",
        "last": "Meier",
        "department": "",
    }

    # Assert result #4
    # (1) Amtsanwältin (beauftragt)
    # (2) Buchstabe `å`
    assert PERSON.search("Tråutwein, Trude, AA'in (ba)").groupdict() == {
        "title": "AA'in (ba)",
        "doc": "",
        "first": "Trude",
        "last": "Tråutwein",
        "department": "",
    }

    # Assert result #5
    # (1) Referendar
    # (2) Doktortitel
    # (3) Abteilung (arabisch)
    assert PERSON.search("Dr. Wolf (192), Moritz , Ref").groupdict() == {
        "title": "Ref",
        "doc": "Dr.",
        "first": "Moritz",
        "last": "Wolf",
        "department": "(192)",
    }

    # Assert result #6
    # (1) Referendarin
    # (2) Kein Leerzeichen
    assert PERSON.search("Flohmann(840), Daniela , Ref'in").groupdict() == {
        "title": "Ref'in",
        "doc": "",
        "first": "Daniela",
        "last": "Flohmann",
        "department": "(840)",
    }

    # Assert result #7
    # (1) Oberstaatsanwalt
    # (2) Doktortitel
    # (3) Doppel(vor)name
    assert PERSON.search("Dr. Rost, Claus-Peter , OStA").groupdict() == {
        "title": "OStA",
        "doc": "Dr.",
        "first": "Claus-Peter",
        "last": "Rost",
        "department": "",
    }

    # Assert result #8
    # (1) Erste Staatsanwältin
    # (2) Doppel(nach)name
    assert PERSON.search("Nguyen-Krafft, Antonia, EStA'in").groupdict() == {
        "title": "EStA'in",
        "doc": "",
        "first": "Antonia",
        "last": "Nguyen-Krafft",
        "department": "",
    }

    # Assert result #9
    # Erster Staatsanwalt
    assert PERSON.search("Burgmeister, Adam , EStA").groupdict() == {
        "title": "EStA",
        "doc": "",
        "first": "Adam",
        "last": "Burgmeister",
        "department": "",
    }

    # Assert result #10
    # Amtsanwältin
    assert PERSON.search("Makaroni, Stefanie, AA'in").groupdict() == {
        "title": "AA'in",
        "doc": "",
        "first": "Stefanie",
        "last": "Makaroni",
        "department": "",
    }

    # Assert result #12
    # Regierungsrat als Amtsanwalt
    assert PERSON.search("Dr. Zorn, Karl, RRaAA").groupdict() == {
        "title": "RRaAA",
        "doc": "Dr.",
        "first": "Karl",
        "last": "Zorn",
        "department": "",
    }

    # Assert result #13
    # Oberamtsanwalt
    assert PERSON.search("Albatros, Sebastian , OAA").groupdict() == {
        "title": "OAA",
        "doc": "",
        "first": "Sebastian",
        "last": "Albatros",
        "department": "",
    }

    # Assert result #14
    # Justizoberinspektorin
    assert PERSON.search("Schlaumair, Olga , JOI'in").groupdict() == {
        "title": "JOI'in",
        "doc": "",
        "first": "Olga",
        "last": "Schlaumair",
        "department": "",
    }

    # Assert result #15
    # (1) Amtsanwalt
    # (2) Leerzeichen
    assert PERSON.search("Kandel , Florian , AA").groupdict() == {
        "title": "AA",
        "doc": "",
        "first": "Florian",
        "last": "Kandel",
        "department": "",
    }

    # Assert result #16
    # (1) Amtsanwältin (beauftragt)
    # (2) Buchstabe `ß`
    # (3) Leerzeichen
    assert PERSON.search("Vließmann , Katja , AA'in (ba)").groupdict() == {
        "title": "AA'in (ba)",
        "doc": "",
        "first": "Katja",
        "last": "Vließmann",
        "department": "",
    }

    # Assert result #17
    # Oberamtsanwältin
    assert PERSON.search("Knollinger, Antje , OAA'in").groupdict() == {
        "title": "OAA'in",
        "doc": "",
        "first": "Antje",
        "last": "Knollinger",
        "department": "",
    }

    # Assert result #18
    # (1) Erste Oberamtsanwältin
    # (2) Buchstabe `ä`
    assert PERSON.search("Schmälzle, Käthe , EOAA'in").groupdict() == {
        "title": "EOAA'in",
        "doc": "",
        "first": "Käthe",
        "last": "Schmälzle",
        "department": "",
    }

    # Assert result #19
    # Erster Oberamtsanwalt
    assert PERSON.search("Gansel, Oskar, EOAA").groupdict() == {
        "title": "EOAA",
        "doc": "",
        "first": "Oskar",
        "last": "Gansel",
        "department": "",
    }

    # Assert result #20
    # (1) Oberstaatsanwältin
    # (2) Leerzeichen
    assert PERSON.search("Greiffstädter , Ursula , OStA'in").groupdict() == {
        "title": "OStA'in",
        "doc": "",
        "first": "Ursula",
        "last": "Greiffstädter",
        "department": "",
    }

    # Assert result #21
    # (1) Amtsanwaltsanwärter
    # (2) Buchstabe `é`
    assert PERSON.search("Piqué, Gaetan , AAAnw").groupdict() == {
        "title": "AAAnw",
        "doc": "",
        "first": "Gaetan",
        "last": "Piqué",
        "department": "",
    }

    # Assert result #22
    # Amtsanwaltsanwärterin
    assert PERSON.search("Nägele, Barbara , AAAnw'in").groupdict() == {
        "title": "AAAnw'in",
        "doc": "",
        "first": "Barbara",
        "last": "Nägele",
        "department": "",
    }

    # Assert result #23
    # Justizoberinspektor
    assert PERSON.search("Schlaumair, Aljosha , JOI").groupdict() == {
        "title": "JOI",
        "doc": "",
        "first": "Aljosha",
        "last": "Schlaumair",
        "department": "",
    }

    # Assert result #24
    # (1) Mehrere Sitzungsvertreter:innen
    # (2) Abteilung (römisch)
    # (3) Kein Leerzeichen
    assert [
        item.groupdict()
        for item in PERSON.finditer(
            "Schlupfmeise(IV), Annika, Ref'in Dr. Backenbarth, Kai , EStA"
        )
    ] == [
        {
            "title": "Ref'in",
            "doc": "",
            "first": "Annika",
            "last": "Schlupfmeise",
            "department": "(IV)",
        },
        {
            "title": "EStA",
            "doc": "Dr.",
            "first": "Kai",
            "last": "Backenbarth",
            "department": "",
        },
    ]


def test_get_people():
    """
    Tests 'get_people'
    """

    # Assert result #1
    assert get_people("") == []
    assert get_people("NN Sitzungsvertreter") == []

    # Assert result #2
    assert get_people("Nägele, Barbara , AAAnw'in") == [
        {
            "title": "AAAnw'in",
            "doc": "",
            "first": "Barbara",
            "last": "Nägele",
            "department": "",
        }
    ]

    # Assert result #3
    assert get_people(
        "Schlupfmeise(IV), Annika, Ref'in Dr. Backenbarth, Kai , EStA"
    ) == [
        {
            "title": "Ref'in",
            "doc": "",
            "first": "Annika",
            "last": "Schlupfmeise",
            "department": "(IV)",
        },
        {
            "title": "EStA",
            "doc": "Dr.",
            "first": "Kai",
            "last": "Backenbarth",
            "department": "",
        },
    ]
