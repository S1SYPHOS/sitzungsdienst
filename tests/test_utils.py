"""
This module is part of the 'sitzungsdienst' package,
which is released under GPL-3.0-only license.
"""

from sitzungsdienst.utils import data2hash, flatten, sort_din5007


def test_data2hash():
    """
    Tests 'data2hash'
    """

    # Assert result #1
    assert (
        "99fecba92d36c730851d5a1967a68c0be7e0d0e79145ac0049fc6680279703e2ecd4"
        + "325c9fb469acf7d05b758f97e7b24f2482991545f987e21773d94e05afdd"
        == data2hash("string1")
    )

    # Assert result #2
    assert (
        "6d13aefb9edf3ea9075d2a0b77df5139cad99ebb3115e413570cc9409405eb205"
        + "f6c5fe0a88b22c5bd2e77afd7bb94bfc4869eb39c2daaaca3a63743fa7c15eb"
        == data2hash(["string1", "string2"])
    )

    # Assert result #3
    assert (
        "0032a3754a5e48f34b9cee4948aa6f5ce1a9ac598f0812349e09db8511e4c6e"
        + "2b8daaac2a966fbf5839dcf98ab83a93be38419fd7d9538dfa33c3e88fac5923e"
        == data2hash({"string1": "string2"})
    )


def test_flatten():
    """
    Tests 'flatten'
    """

    # Assert result #1
    assert ["a", "b", "c", "d"] == flatten([["a", "b", "c", "d"]])

    # Assert result #2
    assert ["a", "b", "c", "d"] == flatten([["a", "b"], ["c"], ["d"]])

    # Assert result #3
    assert [["a", "b"], ["c", "d"]] == flatten([[["a", "b"], ["c", "d"]]])


def test_sort_din5007():
    """
    Tests 'sort_din5007'
    """

    # Assert result
    assert ["Äb", "Ac", "Ba"] == sorted(["Ac", "Ba", "Äb"], key=sort_din5007)
